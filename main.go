package main

import (
	"fmt"
	"log"
	"flag"
	"path/filepath"
	"os/user"
	"os"
	"strings"
	"os/exec"
	"time"
)

type Options struct {
	Host string
	RemotePort string
	RemoteListenPort string
	SSHKeyPath string
	RemoteHost string
	RemoteUser string
}

func defaultSSHKeyPath() string {
	usr, _ := user.Current()
	return filepath.Join(usr.HomeDir, ".ssh", "id_rsa")
}

func parseArgs() Options {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "%s <remotehost> <remoteuser>\n", os.Args[0])
		flag.PrintDefaults()
	}
	options := Options{}
	flag.StringVar(&options.RemotePort, "remotePort", "22", "Port to ssh to on remote server")
	flag.StringVar(&options.RemoteListenPort, "remoteListenPort", "2222", "Port to listen on on remote server")
	flag.StringVar(&options.SSHKeyPath, "key", defaultSSHKeyPath(), "SSH Key path to authenticate with")
	flag.Parse()

	if (flag.NArg() != 2) {
		flag.Usage()
		os.Exit(1)
	}
	options.RemoteHost = flag.Arg(0)
	options.RemoteUser = flag.Arg(1)

	return options
}

func main() {
	options := parseArgs()

	fmt.Println("Remote Port: ", options.RemotePort)
	fmt.Println("Remote Listen Port: ", options.RemoteListenPort)
	fmt.Println("SSH Key path: ", options.SSHKeyPath)
	fmt.Println("Remote Host: ", options.RemoteHost)
	fmt.Println("Remote User: ", options.RemoteUser)

	c := []string{"ssh", "-i", defaultSSHKeyPath(), "-f", "-N", "-l", options.RemoteUser, options.RemoteHost, "-p", options.RemotePort, "-R", fmt.Sprintf("%s:localhost:22", options.RemoteListenPort)}

	for {
		log.Printf("Establishing connection to %s:%s", options.RemoteHost, options.RemotePort)
		log.Printf(strings.Join(c, " "))
		cmd := exec.Command(c[0], c[1:]...)
		stdoutStderr, err := cmd.CombinedOutput()
		if err != nil {
			log.Fatal("Error in connection: ", err)
		}
		log.Println(stdoutStderr)
		log.Println("Looks like the connection was disconnected. Retrying...")
		time.Sleep(10 * time.Second)
	}
}
