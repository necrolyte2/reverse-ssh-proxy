# reverse-ssh-proxy

Reverse ssh proxy in GoLang for learning purposes

# Usage

```
./reverse-ssh-proxy <remotehost> <remoteuser>
  -key string
        SSH Key path to authenticate with (default "/home/.../.ssh/id_rsa")
  -remoteListenPort string
        Port to listen on on remote server (default "2222")
  -remotePort string
        Port to ssh to on remote server (default "22")
```

# Example

- servera.example.com has username `usera`
- serverb.example.com has username `userb`

1. On servera.example.com setup an ssh key to use for authentication

    ```
    ssh-keygen -f ~/.ssh/reverseproxy
    ```

1. Setup both servers to allow this key for your user

     Run the following commands on usera@servera.example.com

     ```
     cat ~/.ssh/reverseproxy.pub >> ~/.ssh/authorized_keys
     ```
     ```
     cat ~/.ssh/reverseproxy.pub | ssh userb@serverb.example.com "mkdir -p ~/.ssh; cat - >> ~/.ssh/authorized_keys"
     cat ~/.ssh/reverseproxy | ssh -i ~/.ssh/reverseproxy userb@serverb.example.com "cat - > ~/.ssh/reverseproxy"
     ```

1. Download [reverse-ssh-proxy](https://gitlab.com/necrolyte2/reverse-ssh-proxy/tags)
1. Run reverse-ssh-proxy on servera.example.com

    ```
    ./reverse-ssh-proxy serberb.example.com userb -key /home/$USER/.ssh/reverseproxy
    ```

1. Now from serverb@example.com you can ssh to servera.example.com via localhost:2222

    ```
    ssh -p 2222 usera@localhost
    ```
